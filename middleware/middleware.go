package middleware

import (
	"compress/flate"
	"context"
	"crypto/tls"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/neouse/util/common"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/rs/cors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	// QueryPathName for graphql query route
	QueryPathName = "/query"
	// HealthPathName for health check route
	HealthPathName = "/healthz"
	// MetricsPathName for metrics route
	MetricsPathName = "/metrics"

	headerTransportDetailLevel = "x-transport-detaillevel"
	headerCodeLogLevel         = "x-code-loglevel"
	fieldRequestID             = "requestID"
	fieldRequestBody           = "requestBody"
	fieldDetailLevel           = "detailLevel"
	fieldStatus                = "status"
	fieldDuration              = "duration"
	fieldRemoteAddr            = "remoteAddr"
	fieldRequestURL            = "requestURL"
	fieldMethod                = "method"
)

// NewHTTPClient returns a new http.Client from proxycafile and baseURL
func NewHTTPClient(logger *zap.SugaredLogger, proxyCAFilename, baseURL string) (*http.Client, error) {
	parsedBaseURL, err := url.Parse(baseURL)
	if err != nil {
		logger.Errorw("Could not parse base URL", "baseURL", baseURL, "err", err)
	}

	var transportProxy func(*http.Request) (*url.URL, error)
	proxy, err := http.ProxyFromEnvironment(&http.Request{URL: parsedBaseURL})
	if err == nil {
		if proxy != nil {
			logger.Infow("Using proxy from environment", "proxy", proxy)
		}
		transportProxy = http.ProxyURL(proxy)
	}

	var transportTLSClientConfig *tls.Config
	rootCAs, err := common.AppendToSystemRootCAs(proxyCAFilename)
	if err == nil {
		if proxyCAFilename != ""{
			logger.Infow("Trusting additional CAs", "file", proxyCAFilename)
		}
		transportTLSClientConfig = &tls.Config{
			RootCAs: rootCAs,
		}
	}

	hc := &http.Client{
		Timeout: time.Second * 20,
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout: 2 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 2 * time.Second,
			TLSClientConfig:     transportTLSClientConfig,
			MaxIdleConnsPerHost: 32,
			Proxy:               transportProxy,
		},
	}

	return hc, err
}

// NewRouterWithMiddleware returns a chi router with attached middleware (metrics, logging, gzip, cors)
func NewRouterWithMiddleware(
	lg LoggerGroup,
	originURLs string,
	transportDetailLevel TransportDetailLevel,
	codeLogLevel CodeLogLevel,
	httpResponseTimesUpdater func(urlPath string, seconds float64),
	httpRequestsUpdater func(urlPath string),
) *chi.Mux {
	router := chi.NewRouter()

	router.Use(addPrometheus(httpRequestsUpdater))
	router.Use(middleware.RequestID)

	if transportDetailLevel.IsValid() && transportDetailLevel != TransportDetailNone {
		router.Use(addTransportDetailer(lg.GetLoggerForLevel(LogLevelInfo), transportDetailLevel))
	}
	router.Use(addCodeLogger(
		lg,
		codeLogLevel,
		httpResponseTimesUpdater,
	))

	// gzip
	compressor := middleware.NewCompressor(flate.DefaultCompression)
	router.Use(compressor.Handler())

	// cors
	router.Use(cors.New(cors.Options{
		AllowedOrigins:   strings.Split(originURLs, " "),
		AllowCredentials: true,
		// Debug:            true,
	}).Handler)
	return router
}

// GetHeader returns the first value found in the first matching header, otherwise returns defaultValue
func GetHeader(headers http.Header, headerName string, defaultValue string) string {
	v := headers.Get(headerTransportDetailLevel)
	if v != "" {
		return v
	}
	return defaultValue
}

func addTransportDetailer(loggerInfo *zap.Logger, transportDetailLevel TransportDetailLevel) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()

			var requestID string
			if reqID := r.Context().Value(middleware.RequestIDKey); reqID != nil {
				requestID = reqID.(string)
			}
			// Allow override of detail levels via inbound HTTP headers
			transportDetailLevel = GetDefaultTransportDetailLevel(
				GetHeader(
					r.Header,
					headerTransportDetailLevel,
					transportDetailLevel.String(),
				),
			)

			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)
			next.ServeHTTP(ww, r)

			duration := time.Since(start)
			fields := []zapcore.Field{
				zap.String(fieldDetailLevel, transportDetailLevel.String()),
				zap.Int(fieldStatus, ww.Status()),
				zap.Duration(fieldDuration, duration),
				zap.String(fieldRemoteAddr, r.RemoteAddr),
				zap.String(fieldRequestURL, r.RequestURI),
				zap.String(fieldMethod, r.Method),
			}

			if requestID != "" {
				fields = append(fields, zap.String(fieldRequestID, requestID))
			}

			// Append the incoming request body if requested
			if transportDetailLevel == TransportDetailFull {
				if req, err := httputil.DumpRequest(r, true); err == nil {
					fields = append(fields, zap.ByteString(fieldRequestBody, req))
				}
			}
			loggerInfo.Info("TransportDetails", fields...)
		})
	}
}

func addCodeLogger(lg LoggerGroup, codeLogLevel CodeLogLevel, updateMetrics func(urlPath string, seconds float64)) func(next http.Handler) http.Handler {
	// Attach a new zap logger instance to each incoming request
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()

			// Derive new sugarLogger for this request context (with requestID)
			var requestID string
			// Allow override of log level via inbound HTTP headers
			logger := lg.GetLoggerForLevel(
				CodeLogLevel(
					GetHeader(
						r.Header,
						headerCodeLogLevel,
						codeLogLevel.String(),
					),
				),
			)
			if reqID := r.Context().Value(middleware.RequestIDKey); reqID != nil {
				requestID = reqID.(string)
			}
			sugarLogger := logger.With(zap.String(fieldRequestID, requestID)).Sugar()
			ctx := context.WithValue(r.Context(), CodeLogger, sugarLogger)
			next.ServeHTTP(w, r.WithContext(ctx))

			updateMetrics(r.URL.Path, time.Since(start).Seconds())
		})
	}
}

func addPrometheus(updateMetrics func(urlPath string)) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			updateMetrics(r.URL.Path)
			next.ServeHTTP(w, r)
		})
	}
}
