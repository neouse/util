package middleware

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	// TransportDetailNone indicates that no transport logging should occur
	TransportDetailNone = TransportDetailLevel("none")
	// TransportDetailBasic indicates that transport without request body should occur
	TransportDetailBasic = TransportDetailLevel("basic")
	// TransportDetailFull indicates that transport logging with request body should occur
	TransportDetailFull = TransportDetailLevel("full")

	// LogLevelWarn enables logging at the warning level
	LogLevelWarn = CodeLogLevel("warn")
	// LogLevelInfo enables logging for warning and info levels
	LogLevelInfo = CodeLogLevel("info")
	// LogLevelDebug enables logging for debug, warning and info levels
	LogLevelDebug = CodeLogLevel("debug")

	// CodeLogger is the context variable for the code logger
	CodeLogger = CodeLoggerContextKey("logger")
)

// CodeLoggerContextKey is the type for the code logger context key
type CodeLoggerContextKey string

// TransportDetailLevel is either none, basic or full. Basic does not include body but full does.
type TransportDetailLevel string

func (tdl TransportDetailLevel) String() string {
	return string(tdl)
}

// IsValid returns true if the log level is one of the available log levels
func (tdl TransportDetailLevel) IsValid() bool {
	return tdl == TransportDetailNone || tdl == TransportDetailBasic || tdl == TransportDetailFull
}

// GetDefaultTransportDetailLevel tries to match against a valid transport log level but defaults to basic.
func GetDefaultTransportDetailLevel(transportDetailLevelStr string) TransportDetailLevel {
	tdl := TransportDetailLevel(transportDetailLevelStr)
	if tdl == TransportDetailNone || tdl == TransportDetailFull {
		return tdl
	}
	return TransportDetailBasic
}

// CodeLogLevel is the log level for
type CodeLogLevel string

func (cll CodeLogLevel) String() string {
	return string(cll)
}

//LoggerGroup contains 3 levels of zap loggers
type LoggerGroup interface {
	GetLoggerForLevel(logLevelStr CodeLogLevel) *zap.Logger
}

type loggerGroup struct {
	loggerWarn  *zap.Logger
	loggerInfo  *zap.Logger
	loggerDebug *zap.Logger
}

// NewLoggerGroup create new group of zap loggers (warn,info,debug)
func NewLoggerGroup() LoggerGroup {
	var lg loggerGroup
	config := zap.NewProductionEncoderConfig()
	encoder := zapcore.NewJSONEncoder(config)

	lg.loggerWarn = zap.New(zapcore.NewCore(encoder, zapcore.Lock(os.Stdout), zapcore.WarnLevel), zap.AddCaller())
	lg.loggerInfo = zap.New(zapcore.NewCore(encoder, zapcore.Lock(os.Stdout), zapcore.InfoLevel), zap.AddCaller())
	lg.loggerDebug = zap.New(zapcore.NewCore(encoder, zapcore.Lock(os.Stdout), zapcore.DebugLevel), zap.AddCaller())
	defer lg.loggerWarn.Sync()
	defer lg.loggerWarn.Sync()
	defer lg.loggerWarn.Sync()

	return &lg
}

// GetLoggerForLevel returns zap logger for the requested log level (warn,info,debug)
func (lg *loggerGroup) GetLoggerForLevel(logLevelStr CodeLogLevel) *zap.Logger {
	switch logLevelStr {
	case LogLevelDebug:
		return lg.loggerDebug
	case LogLevelInfo:
		return lg.loggerInfo
	default:
		return lg.loggerWarn
	}
}
