package middleware

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/gorilla/websocket"
)

// NewCORSWebsocketTransport returns a websocket transport that checks
// requests match any of the supplied origin URLs hosts and ports
func NewCORSWebsocketTransport(originURLs string) *transport.Websocket {
	return &transport.Websocket{
		Upgrader: websocket.Upgrader{
			CheckOrigin:     hostPortInOriginsChecker(originURLs),
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		},
	}
}

func hostPortInOriginsChecker(originURLs string) func(r *http.Request) bool {
	return func(r *http.Request) bool {
		if originURLs == "*"{
			return true
		}
		for _, originURL := range strings.Split(originURLs, " ") {
			origin, err := url.Parse(originURL)
			if err != nil && r.URL.Hostname() == origin.Hostname() && r.URL.Port() == origin.Port() {
				return true
			}
		}
		return false
	}
}
