package graphql

import (
	"context"
	"fmt"
	"strings"

	"github.com/99designs/gqlgen/graphql"
)

// IsInternal is a graphql filter that identifies calls for endpoint introspection
func IsInternal(args ...string) bool {
	for _, a := range args {
		if strings.HasPrefix(a, "__") {
			return true
		}
	}
	return false
}

// FieldTracer is a helper for logging field level changes in a graphql request
func FieldTracer(ctx context.Context, next graphql.Resolver) (res interface{}, err error) {
	rc := graphql.GetFieldContext(ctx)
	if !IsInternal(rc.Object, rc.Field.Name) {
		fmt.Println("Entered", rc.Object, rc.Field.Name)
	}
	res, err = next(ctx)
	if !IsInternal(rc.Object, rc.Field.Name) {
		fmt.Println("Left", rc.Object, rc.Field.Name, "=>", res, err)
	}
	return res, err
}
