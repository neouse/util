package common

import (
	"crypto/x509"
	"fmt"
	"io/ioutil"
)

// AppendToSystemRootCAs loads CA certificate from file and appends to pool
func AppendToSystemRootCAs(file string) (*x509.CertPool, error) {
	pool, err := x509.SystemCertPool()
	if file == "" {
		return pool, err
	}
	certs, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	if ok := pool.AppendCertsFromPEM(certs); !ok {
		return nil, fmt.Errorf("Could not append CA certificate (%s) to pool", file)
	}
	return pool, nil
}
