package env

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetEnvDefaultStr(t *testing.T) {
	testEnvName := "testEnvName"
	testValue := "testValue"
	testDefault := "testDefault"

	// Env not set, should return default
	v := GetEnvDefaultStr(testEnvName, "")
	assert.Equal(t, "", v)

	// Env not set, should return default
	v = GetEnvDefaultStr(testEnvName, testDefault)
	assert.Equal(t, testDefault, v)

	// Env set correctly, should return str
	os.Setenv(testEnvName, testValue)
	v = GetEnvDefaultStr(testEnvName, testDefault)
	assert.Equal(t, testValue, v)
}

func Test_GetEnvDefaultInt(t *testing.T) {
	testEnvName := "testEnvName"
	testValue := "99"
	testValueInt := 99
	testDefaultInt := 10

	testBadValue := "notInt"

	// Env not set, should return default
	v := GetEnvDefaultInt(testEnvName, testDefaultInt)
	assert.Equal(t, testDefaultInt, v)

	// Env set correctly, should return int value
	os.Setenv(testEnvName, testValue)
	v = GetEnvDefaultInt(testEnvName, testDefaultInt)
	assert.Equal(t, testValueInt, v)

	// Env set with bad input (non-numeric), should return default
	os.Setenv(testEnvName, testBadValue)
	v = GetEnvDefaultInt(testEnvName, testDefaultInt)
	assert.Equal(t, testDefaultInt, v)
}

func Test_GetEnvDefaultBool(t *testing.T) {
	testEnvName := "testEnvName"
	testValue := "false"
	testValueBool := false
	testDefaultBool := true

	testBadValue := "notBool"

	// Env not set, should return default
	v := GetEnvDefaultBool(testEnvName, testDefaultBool)
	assert.Equal(t, testDefaultBool, v)

	// Env set correctly, should return bool value
	os.Setenv(testEnvName, testValue)
	v = GetEnvDefaultBool(testEnvName, testDefaultBool)
	assert.Equal(t, testValueBool, v)

	// Env set with bad input (not 'true' or 'false'), should return default
	os.Setenv(testEnvName, testBadValue)
	v = GetEnvDefaultBool(testEnvName, testDefaultBool)
	assert.Equal(t, testDefaultBool, v)
}
