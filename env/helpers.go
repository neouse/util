package env

import (
	"os"
	"strconv"

	"go.uber.org/zap"
)

// GetEnvDefaultStr returns the environment variable as a string (if present, otherwise returns defaultValue)
func GetEnvDefaultStr(name, defaultValue string) string {
	v := os.Getenv(name)
	if v == "" {
		return defaultValue
	}
	return v
}

// GetEnvDefaultBool returns the environment variable as a bool (if present, otherwise returns defaultValue)
func GetEnvDefaultBool(name string, defaultValue bool) bool {
	s := os.Getenv(name)

	v, err := strconv.ParseBool(s)
	if err != nil {
		return defaultValue
	}
	return v
}

// GetEnvDefaultInt returns the environment variable as an int (if present, otherwise returns defaultValue)
func GetEnvDefaultInt(name string, defaultValue int) int {
	s := os.Getenv(name)

	v, err := strconv.Atoi(s)
	if err != nil {
		return defaultValue
	}
	return v
}

// MustGetEnv returns the environment variable as a string (fatal if not present)
func MustGetEnv(logger *zap.SugaredLogger, name string) string {
	v := os.Getenv(name)
	if v == "" {
		logger.Fatal("Environment variable not set", "name", name)
	}
	return v
}
