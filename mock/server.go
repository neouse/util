package mock

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"net"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"golang.org/x/net/http2"
)

// ServerMock returns mock responses for the given pattern and handler from
// a HTTP server listening on a system-chosen port on the local loopback
// interface, for use in end-to-end HTTP tests.
func ServerMock(pattern string, h http.HandlerFunc) (baseURL string, mux *http.ServeMux, httpClient *http.Client, teardownFn func()) {
	mux = http.NewServeMux()
	if pattern != "" && h != nil {
		mux.HandleFunc(pattern, h)
	}

	srv := httptest.NewUnstartedServer(mux)
	srv.TLS = &tls.Config{
		CipherSuites: []uint16{tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256},
		NextProtos:   []string{http2.NextProtoTLS},
	}
	srv.StartTLS()

	teardownFn = srv.Close

	baseURL = srv.URL

	pool := x509.NewCertPool()
	pool.AddCert(srv.Certificate())
	httpClient = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: pool,
			},
			DialContext: func(_ context.Context, network, _ string) (net.Conn, error) {
				return net.Dial(network, srv.Listener.Addr().String())
			},
		},
	}

	return
}

// OverrideBaseURL changes the host and scheme of the originalURL to that of
// the baseURL. This is typically used in conjunction with ServerMock.
func OverrideBaseURL(t *testing.T, originalURL, baseURL string) string {
	ou, err := url.Parse(originalURL)
	if err != nil {
		t.Fatal(err)
	}
	bu, err := url.Parse(baseURL)
	if err != nil {
		t.Fatal(err)
	}
	ou.Scheme = bu.Scheme
	ou.Host = bu.Host
	return ou.String()
}
