package mock

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"
)

// NewTestLogger creates a log observer for verifying expected messages
func NewTestLogger() (sugar *zap.SugaredLogger, out *observer.ObservedLogs) {
	observer, out := observer.New(zap.WarnLevel)
	return zap.New(observer).Sugar(), out
}
