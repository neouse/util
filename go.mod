module bitbucket.org/neouse/util

go 1.14

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/gorilla/websocket v1.4.1
	github.com/rs/cors v1.7.0
	github.com/stretchr/testify v1.5.1
	go.uber.org/zap v1.14.0
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a
)
